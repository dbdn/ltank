PROJECT = ltank

ERLC_OPTS ?= +'{parse_transform, lager_transform}'

AUTOPATCH += recon folsom bear

# Dependencies.

DEPS = lager pmod_transform recon poolboy protobuffs folsom cowboy jsx
dep_pmod_transform = git https://github.com/erlang/pmod_transform.git master
dep_recon = git https://github.com/ferd/recon.git master
dep_poolboy = git https://github.com/devinus/poolboy.git master
dep_protobuffs = git http://gitlab.jedi.com/server/erlang_protobuffs.git master
dep_folsom = git https://github.com/boundary/folsom.git master
dep_jsx = git https://github.com/spawnfest/jsx.git beamspirit

include erlang.mk