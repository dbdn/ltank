%%%-------------------------------------------------------------------
%%% @author zyuyou yuyouchow@gmail.com
%%% @copyright (C) 2014, Y.S.
%%% @doc 时间日期相关的库
%%%
%%% @end
%%% Created : 27. Mar 2014 5:50 PM
%%%-------------------------------------------------------------------
-module(lib_time).
-author("zyuyou").

-include("common.hrl").

%% API
-export([now/0, now_sec/0, now_ms/0]).
-export([day_sec/1, hour_sec/1, min_sec/1]).
-export([datetime_to_iso/1, datetime_to_str/1, str_to_datetime/1]).
-export([second_0_to_1970/0, datetime_to_universal_seconds/1, universal_seconds_to_local_datetime/1, str_to_universal_seconds/1]).
-export([is_in_one_day/1, is_in_one_day_by_hour/2, is_in_one_day_by_hour/3]).

-export([date_str/1, date_str/2]).

%% @doc 获取当前时间
now() ->
    erlang:now().

%% @doc return the now in seconds
-spec now_sec() -> pos_integer().
now_sec() ->
    {Mega, Sec, _Micro} = erlang:now(),
    Mega * 1000000 + Sec.

%% @doc return the now timestamp in milliseconds
-spec now_ms() -> pos_integer().
now_ms() ->
    {Mega, Sec, Micro} = erlang:now(),
    (Mega * 1000000 + Sec) * 1000 + (Micro div 1000).

%% @doc convert day to seconds
-spec day_sec(Day :: integer()) -> integer().
day_sec(Day) -> Day * 86400.

%% @doc convert hour to seconds
-spec hour_sec(Hour :: integer()) -> integer().
hour_sec(Hour) -> Hour * 3600.

%% @doc convert minitue to seconds
-spec min_sec(Min :: integer()) -> integer().
min_sec(Min) -> Min * 60.

%% @doc 0年到1970之间的秒数
second_0_to_1970() ->
	62167219200. % 719528 * 86400.

%% @doc time to iso str: 20090827T04:43:14
datetime_to_iso({{Year, Month, Day}, {Hour, Minute, Second}}) ->
	lists:flatten(
		io_lib:format("~4..0B~2..0B~2..0BT~2..0B:~2..0B:~2..0B",
			[Year, Month, Day, Hour, Minute, Second])).

%% @doc time to iso str: 2009-08-27 04:43:14
datetime_to_str({{Year, Month, Day}, {Hour, Minute, Second}}) ->
	lists:flatten(
		io_lib:format("~4..0B-~2..0B-~2..0B ~2..0B:~2..0B:~2..0B",
			[Year, Month, Day, Hour, Minute, Second])).

%% @doc iso str to datetime
str_to_datetime(Str) ->
	[Year, Month, Day, Hour, Minute, Second] = string:tokens(Str, "- :"),
	{
		{list_to_integer(Year), list_to_integer(Month), list_to_integer(Day)},
		{list_to_integer(Hour), list_to_integer(Minute), list_to_integer(Second)}
	}.

%% @doc convert local time to universal seconds
%% DT: {{Year, Month, Day}, {Hour, Minute, Second}}
datetime_to_universal_seconds(DT) ->
	[UT |_] = calendar:local_time_to_universal_time_dst(DT),
	UTC = {{1970, 1, 1}, {0,0,0}},
	calendar:datetime_to_gregorian_seconds(UT) - calendar:datetime_to_gregorian_seconds(UTC).

%% @doc convert universal seconds to gregorian datetime
%% Secs: universal seconds
universal_seconds_to_local_datetime(Secs) ->
	Seconds = Secs + second_0_to_1970(),
	calendar:gregorian_seconds_to_datetime(Seconds).

%% @doc convert iso str to universal seconds
%% Str: "yyyy-MM-dd HH:mm:SS"
str_to_universal_seconds(Str) ->
	DT = str_to_datetime(Str),
	datetime_to_universal_seconds(DT).

%% @doc 是否处在同一天(根据判断时间)
is_in_one_day(TimeSecA) ->
	is_in_one_day_by_hour(TimeSecA, now_sec(), 0).

%% 判断时间顺延x小时
is_in_one_day_by_hour(TimeSec, Hour) ->
	is_in_one_day_by_hour(TimeSec, now_sec(), Hour).

is_in_one_day_by_hour(TimeSecA, TimeSecB, Hour) ->
	(TimeSecA - hour_sec(Hour)) div day_sec(1) =:= (TimeSecB - hour_sec(Hour)) div day_sec(1).

date_str({Date,Time}) ->
	date_str(Date, Time).
date_str({Y,Mo,D}=Date,{H,Mi,S}=Time) ->
	case application:get_env(sasl,utc_log) of
		{ok,true} ->
			{{YY,MoMo,DD},{HH,MiMi,SS}} =
				local_time_to_universal_time({Date,Time}),
			lists:flatten(io_lib:format("~w-~2.2.0w-~2.2.0w ~2.2.0w:"
			"~2.2.0w:~2.2.0w UTC",
				[YY,MoMo,DD,HH,MiMi,SS]));
		_ ->
			lists:flatten(io_lib:format("~w-~2.2.0w-~2.2.0w ~2.2.0w:"
			"~2.2.0w:~2.2.0w",
				[Y,Mo,D,H,Mi,S]))
	end.

local_time_to_universal_time({Date,Time}) ->
	case calendar:local_time_to_universal_time_dst({Date,Time}) of
		[UCT] ->
			UCT;
		[UCT1,_UCT2] ->
			UCT1;
		[] -> % should not happen
			{Date,Time}
	end.
