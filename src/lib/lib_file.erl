%%%-------------------------------------------------------------------
%%% @author zyuyou yuyouchow@gmail.com
%%% @copyright (C) 2014, Y.S.
%%% @doc
%%%
%%% @end
%%% Created : 20. Mar 2014 9:15 PM
%%%-------------------------------------------------------------------
-module(lib_file).
-author("zyuyou").

-include("lib.hrl").
-include_lib("kernel/include/file.hrl").

%% API
-export([is_dir/1, is_regular/1, is_file/1, read_file_info/1]).
-export([consult_nested/1, consult_nested/3]).


%% @doc 嵌套解析erlang term文件
consult_nested(File) ->
    case catch consult_nested([File], [], []) of
        {error, _} = Ret ->
            Ret;
        {Terms, _} ->
            {ok, Terms}
    end.

%% @doc 嵌套解析erlang term 文件
consult_nested([], Terms, Already) ->
    {Terms, Already};
consult_nested([FileName | RestFileList], Terms, Already) ->
    Config =
        case file:consult(FileName) of
            {ok, L} ->
                L;
            Error ->
                %?ERROR("the ~p file format error!~n", [FileName]),
                throw(Error)
        end,

    Already2 = [FileName | Already],
    {Terms2, Already3} =
        lists:mapfoldl(
            fun
                ({include, Val}, Acc) ->
                    % 子配置文件
                    consult_nested(do_expand_files(FileName, Val, Already2), [], Acc);
                (_Other, Acc) ->
                    {_Other, Acc}
            end, Already2, Config),

    consult_nested(RestFileList, lists:flatten([Terms | Terms2]), Already3).

%% @doc 获取子配置文件
do_expand_files(FileName, Val0, Already) ->
    DirName = filename:dirname(FileName),
    Val = filename:join([DirName, Val0]),
    lists:foldl(
        fun(F, Acc) ->
            case lists:member(F, Already) of
                true ->
                    Acc;
                false ->
                    [F | Acc]
            end
        end, [], filelib:wildcard(Val)).

%% @doc 是否为目录
is_dir(File) ->
    case prim_file:read_file_info(File) of
        {ok, #file_info{type = directory}} ->
            true;
        _ ->
            false
    end.

%% @doc 是否为常规文件
is_regular(File) ->
    case prim_file:read_file_info(File) of
        {ok, #file_info{type = regular}} ->
            true;
        _ ->
            false
    end.

%% @doc 是否为文件或目录(同filelib:is_file/1)
is_file(File) ->
    case prim_file:read_file_info(File) of
        {ok, #file_info{type = regular}} ->
            true;
        {ok, #file_info{type = directory}} ->
            true;
        _ ->
            false
    end.

%% @doc 读取文件信息
read_file_info(File) ->
    prim_file:read_file_info(File).