%%%-------------------------------------------------------------------
%%% @author zyuyou yuyouchow@gmail.com
%%% @copyright (C) 2014, Y.S.
%%% @doc 网络相关的库
%%%
%%% @end
%%% Created : 21. Mar 2014 2:14 PM
%%%-------------------------------------------------------------------
-module(lib_net).
-author("zyuyou").

%% API
-export([ipv6_supported/0]).
-export([do_active/1]).
-export([peer_str/1, peer/1]).

-export([ip_to_num/1, ip_to_tuple/1]).

%% 是否支持ip6
ipv6_supported() ->
    case (catch inet:getaddr("localhost", inet6)) of
        {ok, _Addr} ->
            true;
        {error, _} ->
            false
    end.

%% @doc 重新设置active
do_active(Sock) ->
    ok = inet:setopts(Sock, [{active, once}]).

%% @doc 获取socket peer的ip
peer_str(Sock) when is_port(Sock) ->
    inet_parse:ntoa(peer(Sock)).

%% @doc 获取socket peer的ip
peer(Sock) when is_port(Sock) ->
    case inet:peername(Sock) of
        {ok, {Addr, _Port}} ->
            Addr;
        {error, Error} ->
            throw({error, Error})
    end.

%% @doc 将ip转换为数字
ip_to_num(Ip) ->
    case catch inet_parse:address(Ip) of
        {ok, {D1, D2, D3, D4}} ->
            <<N:32>> = <<D1, D2, D3, D4>>,
            N;
        _ ->
            0
    end.

%% @doc 将ip转换为tuple
ip_to_tuple(Ip) when is_list(Ip) ->
	{ok, Tuple} = inet_parse:address(Ip),
	Tuple;
ip_to_tuple(Ip) when is_tuple(Ip) ->
	Ip.