%%%-------------------------------------------------------------------
%%% @author zyuyou
%%% @copyright (C) 2014, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 09. Sep 2014 下午10:05
%%%-------------------------------------------------------------------
-module(lib_code).

%% API
-export([reload/0]).
-compile([export_all]).

reload() ->
	reload_modules(all_changed()).

reload_modules(Modules) ->
	[begin code:purge(M), code:load_file(M) end || M <- Modules].

%% @spec all_changed() -> [atom()]
%% @doc Return a list of beam modules that have changed.
all_changed() ->
	[M || {M, Fn} <- code:all_loaded(), is_list(Fn), is_changed(M)].

%% @spec is_changed(atom()) -> boolean()
%% @doc true if the loaded module is a beam with a vsn attribute
%%      and does not match the on-disk beam file, returns false otherwise.
is_changed(M) ->
	try
		module_vsn(M:module_info()) =/= module_vsn(code:get_object_code(M))
	catch _:_ ->
		false
	end.

%% 获取模块vsn
module_vsn({M, Beam, _Fn}) ->
	{ok, {M, Vsn}} = beam_lib:version(Beam),
	Vsn;
module_vsn(L) when is_list(L) ->
	{_, Attrs} = lists:keyfind(attributes, 1, L),
	{_, Vsn} = lists:keyfind(vsn, 1, Attrs),
	Vsn.
