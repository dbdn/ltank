%%%-------------------------------------------------------------------
%%% @author zyuyou yuyouchow@gmail.com
%%% @copyright (C) 2014, Y.S.
%%% @doc
%%%
%%% @end
%%% Created : 21. Mar 2014 1:20 PM
%%%-------------------------------------------------------------------
-module(lib_fun).
-author("zyuyou").

-include("common.hrl").

%% API
-export([is_exported/1, is_exported/3]).

%% @doc 函数是否已经导出
is_exported(M, F, Arity) ->
    is_exported({M, F, Arity}).

%% @doc 函数是否已经导出
is_exported({M, F, ArityCount}) ->
    Exports = M:module_info(exports),
    lists:member({F, ArityCount}, Exports).


