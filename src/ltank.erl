%%%-------------------------------------------------------------------
%%% @author zyuyou yuyouchow@gmail.com
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. Jan 2015 下午1:21
%%%-------------------------------------------------------------------
-module(ltank).

%% API
-export([start/0, stop/0]).

%% @doc start the ltank
start() ->
	ltank_deps:ensure_app(),

	lager:start(),

	ok = application:start(ltank).

%% @doc stop the ltank
stop() ->
	application:stop(ltank).