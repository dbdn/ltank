%%%-------------------------------------------------------------------
%%% @author root
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 16. Jan 2015 下午1:25
%%%-------------------------------------------------------------------
-module(ltank_deps).
-author("root").

%% API
-export([ensure_app/0]).

%% @doc ensure all deps app started
ensure_app() ->
	lib_app:ensure_started(crypto),
	lib_app:ensure_started(sasl),
	lib_app:ensure_started(ranch),
	lib_app:ensure_started(cowlib),
	lib_app:ensure_started(cowboy),

	ok.