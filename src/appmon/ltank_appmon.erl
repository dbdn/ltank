%%%-------------------------------------------------------------------
%%% @author zyuyou yuyouchow@gmail.com
%%% @copyright (C) 2015, <COMPANY>
%%% @doc created from bigwig_xxx
%%%
%%% @end
%%% Created : 16. Jan 2015 下午3:04
%%%-------------------------------------------------------------------
-module(ltank_appmon).

-behaviour(gen_server).

-include("ltank.hrl").

%% API
-export([start_link/0, stop/0]).

%% gen_server callbacks
-export([init/1,
	handle_call/3,
	handle_cast/2,
	handle_info/2,
	terminate/2,
	code_change/3]).

-define(SERVER, ?MODULE).

-record(mnode, {
	name :: atom(),
	status :: [alive | dead],
	pid :: pid(), % info pid
	apps :: [{pid(), atom(), list()}],
	load :: {non_neg_integer(), non_neg_integer()}
}).

-record(state, {
	mnodes = [] :: [#mnode{}]
}).

%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% Starts the server
%%
%% @end
%%--------------------------------------------------------------------
start_link() ->
	gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

stop() ->
	gen_server:cast(?MODULE, stop).

%%%===================================================================
%%% gen_server callbacks
%%%===================================================================

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Initializes the server
%%
%% @spec init(Args) -> {ok, State} |
%%                     {ok, State, Timeout} |
%%                     ignore |
%%                     {stop, Reason}
%% @end
%%--------------------------------------------------------------------
init([]) ->
	process_flag(trap_exit, true),
	net_kernel:monitor_nodes(true),

	NodesOk = lists:filter(fun check_node/1, nodes()),

	Nodes = [node()| NodesOk],

	{ok, #state{mnodes = [monitor_node(N) || N <- Nodes]}}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling call messages
%%
%% @end
%%--------------------------------------------------------------------
handle_call(nodes, _From, #state{mnodes = M} = State) ->
	{reply, [lists:zip(record_info(fields, mnode), tl(tuple_to_list(X))) || X <- M], State};
handle_call(_Request, _From, State) ->
	{reply, badarg, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling cast messages
%%
%% @end
%%--------------------------------------------------------------------
handle_cast(stop, State) ->
	{stop, normal, State};
handle_cast(_Request, State) ->
	{noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Handling all non call/cast messages
%%
%% @spec handle_info(Info, State) -> {noreply, State} |
%%                                   {noreply, State, Timeout} |
%%                                   {stop, Reason, State}
%% @end
%%--------------------------------------------------------------------
handle_info({statistics, L}, State) ->
	send({statistics, L}),
	{noreply, State};
handle_info({node_apps, _} = Msg, State) ->
	send(Msg),
	{noreply, State};
handle_info({node_app_tree, _} = Msg, State) ->
	send(Msg),
	{noreply, State};
handle_info({nodeup, Node}, State) ->
	case check_node(Node) of
		true ->
			case get_mnode(Node, State#state.mnodes) of
				false ->
					send({new_node, Node});
				true ->
					ok
			end,
			MNode = monitor_node(Node),
			MNode = replace_mnode(Node, MNode, State#state.mnodes),
			send({node_up, Node}),
			{noreply, State#state{mnodes = MNode}};
		false ->
			{noreply, State}
	end;
handle_info({nodedown, Node}, State) ->
	case get_mnode(Node, State#state.mnodes) of
		false ->
			{noreply, State};
		MNode ->
			MNode1 = MNode#mnode{status = dead},
			MNodes = replace_mnode(Node, MNode1, State#state.mnodes),
			send({node_down, Node}),
			{noreply, State#state{mnodes = MNodes}}
	end;
handle_info({'EXIT', _Pid, Reason}, State) ->
	case Reason of
		shutdown ->
			{stop, Reason, State};
		_ ->
			{noreply, State}
	end;
handle_info(_Info, State) ->
%% 	?ERROR("unknown info:~p", [_Info]),
	{noreply, State}.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% This function is called by a gen_server when it is about to
%% terminate. It should be the opposite of Module:init/1 and do any
%% necessary cleaning up. When it returns, the gen_server terminates
%% with Reason. The return value is ignored.
%%
%% @spec terminate(Reason, State) -> void()
%% @end
%%--------------------------------------------------------------------
terminate(_Reason, _State) ->
	ok.

%%--------------------------------------------------------------------
%% @private
%% @doc
%% Convert process state when code is changed
%%
%% @spec code_change(OldVsn, State, Extra) -> {ok, NewState}
%% @end
%%--------------------------------------------------------------------
code_change(_OldVsn, State, _Extra) ->
	{ok, State}.

%%%===================================================================
%%% Internal functions
%%%===================================================================
% Make sure we have this module on the remote node
-spec check_node(atom()) -> boolean().
check_node(Node) -> is_list(rpc:call(Node, code, which, [?MODULE])).

-spec monitor_node(atom()) -> #mnode{}.
monitor_node(Node) ->
	{ok, Pid} = ltank_appmon_info:start_link(Node, self()),
	#mnode{name = Node, status = alive, pid = Pid}.

-spec get_mnode(atom(), [#mnode{}]) -> #mnode{} | false.
get_mnode(Node, MNodes) ->
	case lists:keysearch(Node, #mnode.name, MNodes) of
		{value, MNode} -> MNode;
		false          -> false
	end.

-spec replace_mnode(atom(), #mnode{}, [#mnode{}]) -> [#mnode{}].
replace_mnode(Node, MNode, [#mnode{name=Node} | MNodes]) ->
	[MNode | MNodes];
replace_mnode(Node, MNode, [MNode2 | MNodes]) ->
	[MNode2 | replace_mnode(Node, MNode, MNodes)];
replace_mnode(_Node, MNode, []) ->
	[MNode].

-spec send(any()) -> ok.
send(Term) ->
	ltank_pubsubhub:notify({?MODULE, Term}).